importScripts('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');

var firebaseConfig = {
  apiKey: "AIzaSyDFVfRsC2UrwOnxCpW3jMEZcjhBKTzCa1c",
  authDomain: "doctoronline-8d0a0.firebaseapp.com",
  databaseURL: "https://doctoronline-8d0a0.firebaseio.com",
  projectId: "doctoronline-8d0a0",
  storageBucket: "doctoronline-8d0a0.appspot.com",
  messagingSenderId: "590777944663",
  appId: "1:590777944663:web:5e35716ef89dd53f9b3112",
  measurementId: "G-QZ8LBMWQHQ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();