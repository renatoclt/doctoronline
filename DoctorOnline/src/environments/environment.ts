// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // Accesos a firebase
  firebaseConfig: {
    apiKey: 'AIzaSyDFVfRsC2UrwOnxCpW3jMEZcjhBKTzCa1c',
    authDomain: 'doctoronline-8d0a0.firebaseapp.com',
    databaseURL: 'https://doctoronline-8d0a0.firebaseio.com',
    projectId: 'doctoronline-8d0a0',
    storageBucket: 'doctoronline-8d0a0.appspot.com',
    messagingSenderId: '590777944663',
    appId: '1:590777944663:web:5e35716ef89dd53f9b3112',
    measurementId: 'G-QZ8LBMWQHQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
