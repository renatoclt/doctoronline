import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../servicios/core/auth.service';
import { FirebaseService } from '../servicios/firebase.service';
import { Doctor } from '../dto/doctor.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrodoctor',
  templateUrl: './registrodoctor.component.html',
  styleUrls: ['./registrodoctor.component.scss']
})
export class RegistrodoctorComponent implements OnInit {

  /**
   * Formuluario a guardar los datos del doctor
   */
  formRegistro: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private firebaseService: FirebaseService,
    private router: Router,
  ) {
  }

  /**
   * Creacion del formulario
   */
  ngOnInit() {
    this.formRegistro = this.formBuilder.group(
      {
        apellido: ['', Validators.required],
        ciudad: ['', Validators.required],
        correo: ['', Validators.required],
        cp: ['', Validators.required],
        nombre: ['', Validators.required],
        numero: ['', Validators.required],
        valoracion: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required]
      }
    );
  }

  registroDoctorFirebase() {
    this.authService.register(this.formRegistro.controls.email.value, this.formRegistro.controls.password.value);
    const doctor = new Doctor();
    doctor.apellido = this.formRegistro.controls.apellido.value;
    doctor.ciudad = this.formRegistro.controls.ciudad.value;
    doctor.correo = this.formRegistro.controls.correo.value;
    doctor.cp = this.formRegistro.controls.cp.value;
    doctor.nombre = this.formRegistro.controls.nombre.value;
    doctor.numero = this.formRegistro.controls.numero.value;
    doctor.valoracion = this.formRegistro.controls.valoracion.value;
    this.firebaseService.saveDoctor( doctor );
    this.router.navigateByUrl('/');
  }

}
