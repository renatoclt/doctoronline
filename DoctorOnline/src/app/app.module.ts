import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { IndexComponent } from "./index/index.component";
import { NavbarIndexComponent } from "./navbar-index/navbar-index.component";
import { FooterIndexComponent } from "./footer-index/footer-index.component";
import { ListadoctoresComponent } from "./listadoctores/listadoctores.component";
import { AgendarcitaComponent } from "./agendarcita/agendarcita.component";
import { NavbarPatientComponent } from "./navbar-patient/navbar-patient.component";
import { InterpretarComponent } from "./interpretar/interpretar.component";
import { NavbarDoctorComponent } from "./navbar-doctor/navbar-doctor.component";
import { HomedoctorComponent } from "./homedoctor/homedoctor.component";
import { HomepacienteComponent } from "./homepaciente/homepaciente.component";
import { SinginComponent } from "./singin/singin.component";
import { PagarComponent } from "./pagar/pagar.component";
import { PaymentConfirmationComponent } from "./payment-confirmation/payment-confirmation.component";
import { RegistrodoctorComponent } from "./registrodoctor/registrodoctor.component";
import { Registrodoctor1Component } from "./registrodoctor1/registrodoctor1.component";
import { RegistropacienteComponent } from "./registropaciente/registropaciente.component";
import { IngresarestudioComponent } from "./ingresarestudio/ingresarestudio.component";
import { HttpClientModule } from "@angular/common/http";
import { InsightsDoctorComponent } from "./insights-doctor/insights-doctor.component";
import { EstudioInterpretarComponent } from "./estudio-interpretar/estudio-interpretar.component";
import { LogoutComponent } from './logout/logout.component';
import { InsightsPatientComponent } from './insights-patient/insights-patient.component';

import { ChartsModule } from 'ng2-charts';

// 1. Import the libs you need
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment.prod';

// Error
import {
  ErrorHandlerConfig, ERROR_HANDLER_CONFIG, DEFAULT_ERROR_HANDLER_CONFIG, NgxUtilitarioErrorHandlerModule} from 'ngx-utilitario-rclt';
import { ErrorLogger } from './helpers/errorLogger';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { CanActivateViaAuthGuard } from './guard/CanActivateViaAuthGuard';
import { ErrorComponent } from './error/error.component';

const CustomErrorHandlerConfig: ErrorHandlerConfig = {
  errorHandlerHooks: [
    ErrorLogger.logErrorMessage,
    console.error,
  ]
};


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    NavbarIndexComponent,
    FooterIndexComponent,
    ListadoctoresComponent,
    AgendarcitaComponent,
    NavbarPatientComponent,
    InterpretarComponent,
    NavbarDoctorComponent,
    HomedoctorComponent,
    HomepacienteComponent,
    SinginComponent,
    PagarComponent,
    PaymentConfirmationComponent,
    RegistrodoctorComponent,
    Registrodoctor1Component,
    RegistropacienteComponent,
    IngresarestudioComponent,
    InsightsDoctorComponent,
    EstudioInterpretarComponent,
    LogoutComponent,
    InsightsPatientComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
     // 1. Firebase
     AngularFireModule.initializeApp(environment.firebaseConfig),
     AngularFirestoreModule, // firestore
     AngularFireAuthModule, // auth
     AngularFireStorageModule, // storage
     AngularFireDatabaseModule, //notification
     //Handler-error
     NgxUtilitarioErrorHandlerModule.forRoot()

  ],
  providers: [
    {provide: ERROR_HANDLER_CONFIG, useValue: DEFAULT_ERROR_HANDLER_CONFIG},
    CanActivateViaAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
