export class ErrorLogger {
    public static logErrorMessage({ message }): void {
        console.error(`An error with the following message has occured: ${message}`)
    }
}
