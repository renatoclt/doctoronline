import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/core/auth.service';

@Component({
  selector: 'app-navbar-doctor',
  templateUrl: './navbar-doctor.component.html',
  styleUrls: ['./navbar-doctor.component.scss']
})
export class NavbarDoctorComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  salir() {
    this.authService.logout();
  }

}
