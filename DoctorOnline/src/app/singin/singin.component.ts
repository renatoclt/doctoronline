import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm, FormGroup, FormBuilder, Validators } from "@angular/forms";

import { LoginKeys } from "../login-keys";
import { AuthService } from '../servicios/core/auth.service';

@Component({
  selector: "app-singin",
  templateUrl: "./singin.component.html",
  styleUrls: ["./singin.component.scss"]
})
export class SinginComponent implements OnInit {
  loginKeys: LoginKeys;
  formLogin: FormGroup;
  estado = false;
  constructor(
    private router: Router,
    private auth: AuthService,
    private formBuilder: FormBuilder) {
      this.loginKeys = new LoginKeys(); //Se declara vacio por los simbolos de ? de Alumno.ts
    }
    
    /**
     * Inicializamos el formulario
   */
  ngOnInit() {
    this.formLogin =  this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async login() {
     await this.auth.login(this.formLogin.controls.email.value, this.formLogin.controls.password.value);
     console.log('fasd despues de login');
     if (localStorage.getItem('user')) {
       this.estado = true;
     } else{
       this.estado = false;
     }
  }
}
