import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { FirebaseService } from '../servicios/firebase.service';
import { DoctorService } from '../servicios/doctor.service';

@Component({
  selector: "app-listadoctores",
  templateUrl: "./listadoctores.component.html",
  styleUrls: ["./listadoctores.component.scss"]
})
export class ListadoctoresComponent implements OnInit {

  public doctorsAll: any[];
  public doctors: any;
  public value = { param: "nombre" };
  public url = 'localhost:3000/doctors';
  constructor(private doctorService: DoctorService, private firebase: FirebaseService) { }

  ngOnInit() {
    // this.getJSON().subscribe(response => {
    //   //Esta es la lista de doctores, salen del JSON listDoctors.json
    //   console.log(response);
    //   this.doctors = response;
    //   this.doctorsAll = response;
    // });
    this.doctorService.getllDoctors().subscribe((data: any) => {
      console.log(data);
      this.doctors = data;
      this.doctorsAll = data;
    });
    // this.http.get<any>(this.url).subscribe(data => {
    //   console.log(data);
    // },
    // error => {
    //   console.log(error);
    // });
  }

  public getJSON(): Observable<any> {
    return this.firebase.getDoctor();
    // return this.http.get("./assets/json/listDoctors.json");
  }

  cambio(value) {
    if ( value === '0') {
      this.doctors = this.doctorsAll;
    } else {
      this.doctors = this.doctorsAll.filter(x => x.especialidad === value);
    }
  }
}
