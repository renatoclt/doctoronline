export class Notification {
    message: string;
    user: string;
    userImage: string;
    receiver: string
    constructor() {
        this.message = '';
        this.user = '';
        this.receiver = '';
        // tslint:disable-next-line: max-line-length
        this.userImage = 'https://spectator.imgix.net/content/uploads/2019/07/Max-1.jpg?auto=compress,enhance,format&crop=faces,entropy,edges&fit=crop&w=820&h=550';
    }
}
