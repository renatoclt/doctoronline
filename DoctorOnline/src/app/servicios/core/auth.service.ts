import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Usuario de firebas
   */
  user: User;

  /**
   * Segundos del tiempo de login
   */
  secondTime = 100;

  /**
   * Obserbable si hay un usuario
   */
  private usuarioObservable: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /**
   * variable a la cual te suscribes para obtener los datos de usuario
   */
  public usuario$ = this.usuarioObservable.asObservable();

  /**
   * Al iniciar validaremos si esta conectado o no a firebase
   */
  constructor(
    public afAuth: AngularFireAuth,
    public router: Router
  ) {
    console.log('ingrese a constructor');
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.usuarioObservable.next(true);
      } else {
        localStorage.setItem('user', null);
        this.usuarioObservable.next(false);
      }
    });
  }

  async login(email: string, password: string) {
    try{
      const result = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      console.log(result);
      setTimeout(() => {
        this.logout();
      },  this.secondTime * 1000);
  
      this.router.navigate(['']);
    }
    catch{
    }
  }

  async register(email: string, password: string) {
    console.log('ingrese aregistro', email, password);
    const result = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    console.log('enviar correo');
    // this.sendEmailVerification();
  }

  async sendEmailVerification() {
    await this.afAuth.auth.currentUser.sendEmailVerification();
    this.router.navigate(['admin/verify-email']);
  }

  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
  }

  async logout() {
    await this.afAuth.auth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['signin']);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
}
