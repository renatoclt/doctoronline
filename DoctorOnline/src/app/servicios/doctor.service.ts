import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient) { }

  getllDoctors() {
    const urlapi = "http://localhost:3000/doctors";
    return this.http.get(urlapi);
  }
}
