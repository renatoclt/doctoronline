const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendNotifications = functions.database.ref('/notificaciones/{notificationId}').onWrite((event) => {

    console.log(event);
    // Setup notification
    const NOTIFICATION_SNAPSHOT = event.after._data ;
    const payload = {
        notification: {
            title: `New Message from ${NOTIFICATION_SNAPSHOT.user}!`,
            body: NOTIFICATION_SNAPSHOT.message,
            icon: NOTIFICATION_SNAPSHOT.userImage,
            click_action: `https://localhost:4200`
        }
    }

    console.log('enviar noticacion');
    return admin.database().ref(`/tokens/${NOTIFICATION_SNAPSHOT.receiver}`).once('value').then((data) => {
        if ( !data.val() ) return;
        console.log('data', data);
        console.log('data', data.val());
        return admin.messaging().sendToDevice(data.val(), payload);
      })
    // admin.database()
    //     .ref(`/tokens/${notificationId}`)
    //     .once('value')
    //     .then(token => token.val())
    //     .then(userFcmToken => {
    //         return admin.messaging().sendToDevice(userFcmToken, payload)
    //     })
    //     .then(res => {
    //         console.log("Sent Successfully", res);
    //     })
    //     .catch(err => {
    //         console.log(err);
    //     });

});